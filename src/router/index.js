import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

// 导入对应的路由
const discovery = () => import('../views/01.discovery.vue')
const playlists = () => import('../views/02.playlists.vue')
const songs = () => import('../views/03.songs.vue')
const mvs = () => import('../views/04.mvs.vue')
const result = () => import('../views/05.result.vue')
const playlist = () => import('../views/06.playlist.vue')
const mv = () => import('../views/07.mv.vue')
// const songs = () => import('../views/03.songs.vue')
export default new Router({
  routes: [
    {path: '/',redirect: '/discovery'},
    {path: '/discovery',component: discovery },
    {path: '/playlists',component: playlists},
    {path: '/playlist',component: playlist},
    { path: '/songs', component: songs },
    { path: '/mvs', component: mvs },
    {path: '/mv', component: mv},
    {path: '/result',component: result}
  ]
})
